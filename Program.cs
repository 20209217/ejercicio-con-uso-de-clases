//Crear un programa que simule un banco que tiene 3 clientes que pueden 
//hacer depósitos y retiros. También el banco requiere que al final 
//del día calcule la cantidad de dinero que hay depositado.


using System;

namespace Ejercicio_5_Puntos_
{
    class Cliente
    {
        private string nombre;
        private int monto;
        public Cliente(string nom)
        {
            nombre = nom;
            monto = 0;
        }
        public void Depositar(int m)
        {
            monto = monto + m;
        }
        public void Extraer(int m)
        {
            monto = monto - m;
        }
        public int RetornarMonto()
        {
            return monto;
        }
        public void Imprimir()
        {
            Console.WriteLine(nombre + " tiene depositado la suma de " + monto);
        }
    }
    class Banco
    {
        private Cliente cliente1, cliente2, cliente3;
        public Banco()
        {
            cliente1 = new Cliente("Celeste Aquino Fermin");
            cliente2 = new Cliente("Maria Teresa Danilo");
            cliente3 = new Cliente("Pedro Antonio Cacerez");
        }
        public void Operar()
        {
            cliente1.Depositar(7000);
            cliente2.Depositar(15000);
            cliente3.Depositar(20000);
            cliente1.Extraer(1000);
            cliente2.Extraer(2000);
            cliente3.Extraer(7000);
        }
        public void DepositosTotales()
        {
            int t = cliente1.RetornarMonto() +
                    cliente2.RetornarMonto() +
                    cliente3.RetornarMonto();
            Console.WriteLine("El total de dinero en el banco es:" + t);
            cliente1.Imprimir();
            cliente2.Imprimir();
            cliente3.Imprimir();
        }
        static void Main(string[] args)
        {
            Banco banco1 = new Banco();
            banco1.Operar();
            banco1.DepositosTotales();
        }
    }
}
